
// To check if a library is compiled with CocoaPods you
// can use the `COCOAPODS` macro definition which is
// defined in the xcconfigs so it is available in
// headers also when they are imported in the client
// project.


// Colours
#define COCOAPODS_POD_AVAILABLE_Colours
#define COCOAPODS_VERSION_MAJOR_Colours 5
#define COCOAPODS_VERSION_MINOR_Colours 5
#define COCOAPODS_VERSION_PATCH_Colours 0

// Realm
#define COCOAPODS_POD_AVAILABLE_Realm
#define COCOAPODS_VERSION_MAJOR_Realm 0
#define COCOAPODS_VERSION_MINOR_Realm 84
#define COCOAPODS_VERSION_PATCH_Realm 0

// Realm/Headers
#define COCOAPODS_POD_AVAILABLE_Realm_Headers
#define COCOAPODS_VERSION_MAJOR_Realm_Headers 0
#define COCOAPODS_VERSION_MINOR_Realm_Headers 84
#define COCOAPODS_VERSION_PATCH_Realm_Headers 0

// STK
#define COCOAPODS_POD_AVAILABLE_STK
#define COCOAPODS_VERSION_MAJOR_STK 4
#define COCOAPODS_VERSION_MINOR_STK 5
#define COCOAPODS_VERSION_PATCH_STK 2

// TheAmazingAudioEngine
#define COCOAPODS_POD_AVAILABLE_TheAmazingAudioEngine
#define COCOAPODS_VERSION_MAJOR_TheAmazingAudioEngine 1
#define COCOAPODS_VERSION_MINOR_TheAmazingAudioEngine 4
#define COCOAPODS_VERSION_PATCH_TheAmazingAudioEngine 3

