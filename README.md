# Pacik for iPhone

This project aims to bring talempong — an Indonesian traditional percussion — to the mobile device. It will explore technologies embedded in iPhone device to simulate the experience of playing the actual instrument. The ubiquitous iPhone is suitable for introducing the instrument to wider audience.

For this study, we collected the motion data from participants who plays ‘imaginary’ talempong with an iPhone as its mallet. The collected data are analysed to develop an algorithm for a real-time hit detection. Features from the detected hits are extracted and mapped to a sound engine to synthesize a hit sound. The implemented system are then evaluated for future improvements and usage.
