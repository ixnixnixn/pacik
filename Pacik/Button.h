//
//  Button.h
//  Pacik
//
//  Created by Ikhsan Assaat on 9/2/14.
//  Copyright (c) 2014 Ikhsan Assaat. All rights reserved.
//

@import SpriteKit;

extern NSString *const kButtonName;

@interface Button : SKLabelNode

+ (instancetype)createButtonWithText:(NSString *)text;

@end
