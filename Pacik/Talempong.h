//
//  Talempong.h
//  Pacik Lab
//
//  Created by Ikhsan Assaat on 8/26/14.
//  Copyright (c) 2014 Ikhsan Assaat. All rights reserved.
//

#import <Foundation/Foundation.h>

@class AEBlockChannel;
@class Hit;

typedef NS_ENUM(NSInteger, TalempongNoteRange) {
    TalempongNoteRangeLowest = -2,
    TalempongNoteRangeLow,
    TalempongNoteRangeNormal,
    TalempongNoteRangeHigh,
    TalempongNoteRangeHighest
};

@interface Talempong : NSObject

@property (nonatomic, readonly) AEBlockChannel *channel;
@property (nonatomic) NSUInteger note;
@property (nonatomic) TalempongNoteRange noteRange;
@property (nonatomic) double stickHardness;
@property (nonatomic) double vibrato;

- (instancetype)initWithNote:(NSInteger)note;
- (void)strikeWithHit:(Hit *)hit;
- (void)strike:(double)amplitude;
- (void)saveParametersToPreset:(NSInteger)presetId;
- (BOOL)applyParametersFromPreset:(NSInteger)presetId;

@end
