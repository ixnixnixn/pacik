//
//  Mixer.h
//  Pacik
//
//  Created by Ikhsan Assaat on 9/2/14.
//  Copyright (c) 2014 Ikhsan Assaat. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Hit;

@interface AudioEngine : NSObject

@property (nonatomic, strong, readonly) NSMutableArray *talempongs;

+ (instancetype)sharedEngine;

- (void)setTalempongWithNotes:(NSArray *)notes;
- (void)previewTalempongNote:(NSUInteger)number;

@end
