//
//  Mixer.m
//  Pacik
//
//  Created by Ikhsan Assaat on 9/2/14.
//  Copyright (c) 2014 Ikhsan Assaat. All rights reserved.
//

#import "AudioEngine.h"
#import "AEAudioController.h"
#import "Talempong.h"
#import "Hit.h"

@interface AudioEngine ()

@property (nonatomic, strong) NSMutableArray *talempongs;
@property (nonatomic, strong) AEAudioController *audioController;

@end

@implementation AudioEngine

+ (instancetype)sharedEngine {
    static dispatch_once_t onceToken;
    static AudioEngine *sharedEngine = nil;
    dispatch_once(&onceToken, ^{
        sharedEngine = [AudioEngine new];
    });
    
    return sharedEngine;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        self.audioController = [[AEAudioController alloc] initWithAudioDescription:[AEAudioController nonInterleavedFloatStereoAudioDescription] inputEnabled:NO];
        
        [self startAudioEngine];
        [self talempongSetup];
    }
    return self;
}

- (void)startAudioEngine {
    NSError *errorAudioSetup;
    BOOL result = [self.audioController start:&errorAudioSetup];
    if (!result) {
        NSLog(@"Error starting audio engine: %@", errorAudioSetup.localizedDescription);
    }
}

- (void)talempongSetup {
    self.talempongs = [NSMutableArray new];
    NSMutableArray *channels = [NSMutableArray new];
    
    for (int i = 1; i <= 2; i++) {
        Talempong *talempong = [[Talempong alloc] initWithNote:i];
        [self.talempongs addObject:talempong];
        [channels addObject:talempong.channel];
    }
    
    [self.audioController addChannels:channels];
}

#pragma mark -

- (void)setTalempongWithNotes:(NSArray *)notes {
    [notes enumerateObjectsUsingBlock:^(NSNumber *note, NSUInteger idx, BOOL *stop) {
        Talempong *talempong = (Talempong *)self.talempongs[idx];
        talempong.note = note.integerValue;
    }];    
}

- (void)previewTalempongNote:(NSUInteger)number {
    Hit *hit = [Hit new];
    hit.amplitude = 0.8;
    
    Talempong *talempong = (Talempong *)self.talempongs[(number % 2)];
    talempong.note = number;
    [talempong strikeWithHit:hit];
}

@end
