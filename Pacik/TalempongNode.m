//
//  TalempongNode.m
//  Circles
//
//  Created by Ikhsan Assaat on 9/1/14.
//  Copyright (c) 2014 Ikhsan Assaat. All rights reserved.
//

#import "TalempongNode.h"
#import "Colours.h"
#import "UIColor+notes.h"

NSString *const kTalempongName = @"talempong";
static CGFloat const kNoteSize = 150;

@interface TalempongNode ()

@property (nonatomic) CGFloat duration;

@end

@implementation TalempongNode

+ (instancetype)createNoteWithNumber:(NSInteger)noteNumber atPoint:(CGPoint)position {
    TalempongNode *note = [super spriteNodeWithImageNamed:@"hit"];
    note.name = kTalempongName;
    
    note.position = position;
    note.size = CGSizeMake(kNoteSize, kNoteSize);
    note.anchorPoint = CGPointMake(0.5, 0.5);
    
    note.color = [UIColor colorForNote:noteNumber];
    note.colorBlendFactor = 1.0;
    note.blendMode = SKBlendModeAlpha;
    
    // values
    note.duration = 0.1;
    
    return note;
}

- (void)selectNote {
    CGFloat scale = 1.5;
    [self runAction:[SKAction scaleTo:scale duration:self.duration]];
}

- (void)deselectNote {
    CGFloat scale = 1.0;
    [self runAction:[SKAction scaleTo:scale duration:self.duration]];
}

@end
