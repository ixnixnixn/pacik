//
//  Vibrator.h
//  Pacik Lab
//
//  Created by Ikhsan Assaat on 8/26/14.
//  Copyright (c) 2014 Ikhsan Assaat. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Vibrator : NSObject

+ (void)vibrate;
+ (void)vibrateWithAmplitude:(double)amplitude;

@end
