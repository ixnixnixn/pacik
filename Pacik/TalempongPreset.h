//
//  TalempongPreset.h
//  Pacik
//
//  Created by Ikhsan Assaat on 9/3/14.
//  Copyright (c) 2014 Ikhsan Assaat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Realm.h"

@interface TalempongPreset : RLMObject

@property NSInteger presetId;
@property NSInteger note;
@property NSInteger noteRange;
@property double stickHardness;
@property double vibrato;

+ (instancetype)findWithId:(NSInteger)presetId;

@end
