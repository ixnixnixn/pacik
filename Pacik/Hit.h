//
//  Hit.h
//  Pacik Lab
//
//  Created by Ikhsan Assaat on 8/26/14.
//  Copyright (c) 2014 Ikhsan Assaat. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Hit : NSObject

@property (nonatomic) double amplitude;
@property (nonatomic) NSInteger note;

@end
