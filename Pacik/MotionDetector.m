//
//  MotionDetector.m
//  Pacik Lab
//
//  Created by Ikhsan Assaat on 8/26/14.
//  Copyright (c) 2014 Ikhsan Assaat. All rights reserved.
//

#import "MotionDetector.h"

@import CoreMotion;
#import "MotionBuffer.h"
#import "Hit.h"


@interface MotionDetector ()

@property (nonatomic, strong) CMMotionManager *manager;
@property (nonatomic, strong) MotionBuffer *buffer;

@end

@implementation MotionDetector

- (instancetype)init {
    self = [super init];
    if (self) {
        self.buffer = [MotionBuffer new];
        self.manager = [CMMotionManager new];
        [self.manager setDeviceMotionUpdateInterval:0.01];
    }
    return self;
}

- (void)setBufferWidth:(NSInteger)bufferWidth {
    _bufferWidth = bufferWidth;
    self.buffer.width = _bufferWidth;
}

- (void)startWithDetectionBlock:(void(^)(Hit *hit))detectionBlock {
    NSOperationQueue *queue = [NSOperationQueue new];
    queue.maxConcurrentOperationCount = 1;
    
    __block NSTimeInterval skipTimestamp = 0;
    
    [self.manager startDeviceMotionUpdatesToQueue:queue withHandler:^(CMDeviceMotion *motion, NSError *error) {
        
    }];
    [self.manager startDeviceMotionUpdatesUsingReferenceFrame:CMAttitudeReferenceFrameXArbitraryCorrectedZVertical toQueue:queue withHandler:^(CMDeviceMotion *motion, NSError *error) {
        
        [self.buffer push:motion];
        
        if (skipTimestamp <= 0) {
            Hit *hit = [self.buffer detectHit];
            if (hit) {
                [[NSOperationQueue mainQueue] addOperationWithBlock:^{ detectionBlock(hit); }];
                skipTimestamp = 100;
            }
        } else {
            skipTimestamp -= 10;
        }
    }];
}

- (void)stop {
    [self.manager stopDeviceMotionUpdates];
}

@end
