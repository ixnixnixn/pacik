//
//  TalempongPreset.m
//  Pacik
//
//  Created by Ikhsan Assaat on 9/3/14.
//  Copyright (c) 2014 Ikhsan Assaat. All rights reserved.
//

#import "TalempongPreset.h"

@implementation TalempongPreset

+ (instancetype)findWithId:(NSInteger)presetId {
    RLMArray *result = [TalempongPreset objectsWhere:@"presetId == %d", presetId];    
    if (result.count == 1) {
        TalempongPreset *preset = [result firstObject];
        return preset;
    }
    
    return nil;
}

@end
