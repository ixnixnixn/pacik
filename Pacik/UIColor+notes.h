//
//  UIColor+notes.h
//  Pacik
//
//  Created by Ikhsan Assaat on 9/2/14.
//  Copyright (c) 2014 Ikhsan Assaat. All rights reserved.
//

@import UIKit;

@interface UIColor (notes)

+ (UIColor *)colorForNote:(NSUInteger)note;

@end
