//
//  TalempongNode.h
//  Circles
//
//  Created by Ikhsan Assaat on 9/1/14.
//  Copyright (c) 2014 Ikhsan Assaat. All rights reserved.
//

@import SpriteKit;

extern NSString *const kTalempongName;

@interface TalempongNode : SKSpriteNode

+ (instancetype)createNoteWithNumber:(NSInteger)noteNumber atPoint:(CGPoint)position;
- (void)selectNote;
- (void)deselectNote;

@end
