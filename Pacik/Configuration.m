//
//  Configuration.m
//  Pacik
//
//  Created by Ikhsan Assaat on 9/2/14.
//  Copyright (c) 2014 Ikhsan Assaat. All rights reserved.
//

#import "Configuration.h"
#import "Colours.h"
#import "UIColor+notes.h"
#import "AudioEngine.h"
#import "Hit.h"
#import "Talempong.h"

static NSString *const kNoteToggleName = @"NoteToggle";
static NSString *const kNoteKey = @"NoteKey";
static NSString *const kArrowName = @"Arrow";
static NSString *const kPickerIndexKey = @"PickerIndexKey";
static NSString *const kPickerName = @"RangePicker";

static CGFloat const kNoteToggleSize = 60;
static CGFloat const kNotePickerSizeWidth = 75;
static CGFloat const kNotePickerSizeHeight = 40;
static CGFloat const kConfigurationSizeWidth = 500;
static CGFloat const kConfigurationSizeHeight = 600;

typedef NS_ENUM(NSInteger, ConfigState) {
    ConfigStateNone,
    ConfigStateHighlightedFirstArrow,
    ConfigStateHighlightedSecondArrow
};

@interface Configuration ()

@property (nonatomic, strong) SKLabelNode *taskLabel;
@property (nonatomic, strong) NSArray *arrows;
@property (nonatomic, strong) NSMutableArray *noteIndexes;
@property (nonatomic) ConfigState state;
@property (nonatomic, readonly) NSArray *talempongs;

@end

@implementation Configuration

- (NSArray *)talempongs {
    return [[AudioEngine sharedEngine] talempongs];
}

+ (instancetype)sharedConfig {
    static Configuration *sharedConfig = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        sharedConfig = [[self alloc] init];
    });
    return sharedConfig;
}

- (instancetype)init {
    if (self = [super init]) {
        self.userInteractionEnabled = YES;
        self.state = ConfigStateNone;
        self.noteIndexes = [@[@0, @0] mutableCopy];
        
        SKShapeNode *background = [self createBackground];
        [self addChild:background];
        
        // add title
        SKLabelNode *label = [SKLabelNode labelNodeWithFontNamed:@"Avenir-Black"];
        label.text = @"Configuration";
        label.position = CGPointMake(0, 240.0);
        label.fontColor = [UIColor whiteColor];
        [self addChild:label];
        
        // add task
        SKLabelNode *taskLabel = [SKLabelNode labelNodeWithFontNamed:@"Avenir"];
        taskLabel.position = CGPointMake(0, 200.0);
        taskLabel.fontColor = [UIColor whiteColor];
        taskLabel.fontSize = 30.0;
        [self addChild:taskLabel];
        self.taskLabel = taskLabel;
        
        // add arrows
        SKSpriteNode *arrow1 = [self createArrow];
        arrow1.position = CGPointMake(0.0, 120.0);
        [self addChild:arrow1];
        SKSpriteNode *arrow2 = [self createArrow];
        arrow2.position = CGPointMake(0.0, 10.0);
        arrow2.zRotation = M_PI * (1/6.0);
        [self addChild:arrow2];
        self.arrows = @[arrow1, arrow2];
        
        // add notes
        for (int i = 1; i <= 6; i++) {
            SKShapeNode *noteToggle = [self createToggleForNote:i];
            noteToggle.position = CGPointMake(76 * (i - 4) + 8, -150.0);
            [self addChild:noteToggle];
        }
        
        // add description for picking range
        SKLabelNode *rangePickerLabel = [SKLabelNode labelNodeWithFontNamed:@"Avenir"];
        rangePickerLabel.text = @"Select note's range";
        rangePickerLabel.position = CGPointMake(0, -205.0);
        rangePickerLabel.fontColor = [UIColor whiteColor];
        rangePickerLabel.fontSize = 30.0;
        [self addChild:rangePickerLabel];
        
        // add range picker
        for (int i = 1; i <= 5; i++) {
            SKShapeNode *picker = [self createRangePickerForNote:i];
            picker.position = CGPointMake(85 * (i - 3) - 35, -270.0);
            [self addChild:picker];
        }
        
        [self updateUI];
    }
    
    return self;
}

#pragma mark - factories

- (SKShapeNode *)createBackground {
    SKShapeNode *background = [SKShapeNode new];
    CGRect frame = CGRectMake(-(kConfigurationSizeWidth/2), -(kConfigurationSizeHeight/2), kConfigurationSizeWidth, kConfigurationSizeHeight);
    background.path = [UIBezierPath bezierPathWithRoundedRect:frame cornerRadius:20.0].CGPath;
    background.fillColor = [SKColor black25PercentColor];
    background.lineWidth = 4.0;
    background.strokeColor = [SKColor black50PercentColor];
    
    return background;
}

- (SKShapeNode *)createToggleForNote:(NSUInteger)note {
    SKShapeNode *noteToggle = [SKShapeNode new];
    noteToggle.name = kNoteToggleName;
    CGRect frame = CGRectMake(0, 0, kNoteToggleSize, kNoteToggleSize);
    noteToggle.path = [UIBezierPath bezierPathWithOvalInRect:frame].CGPath;
    noteToggle.fillColor = [SKColor colorForNote:note];
    noteToggle.lineWidth = 0.0;
    noteToggle.userData = [@{ kNoteKey:@(note) } mutableCopy];
    
    return noteToggle;
}

- (SKSpriteNode *)createArrow {
    SKSpriteNode *arrow = [SKSpriteNode spriteNodeWithImageNamed:@"arrow"];
    arrow.name = kArrowName;
    arrow.anchorPoint = CGPointMake(0.5, 0.5);
    arrow.color = [SKColor ghostWhiteColor];
    arrow.colorBlendFactor = 1.0;
    arrow.alpha = 0.2;
    
    return arrow;
}

- (SKShapeNode *)createRangePickerForNote:(NSUInteger)note {
    SKShapeNode *picker = [SKShapeNode new];
    picker.name = kPickerName;
    CGRect frame = CGRectMake(0, 0, kNotePickerSizeWidth, kNotePickerSizeHeight);
    picker.path = [UIBezierPath bezierPathWithRoundedRect:frame cornerRadius:2.0].CGPath;
    picker.fillColor = [SKColor watermelonColor];
    picker.lineWidth = 0.0;
    picker.userData = [@{ kPickerIndexKey:@(note - 3) } mutableCopy];
    picker.alpha = 0.5;
    
    return picker;
}

#pragma mark - helpers

- (void)activateToggle:(BOOL)active {
    CGFloat alpha = active? 1.0 : 0.6 ;
    [self.children enumerateObjectsUsingBlock:^(SKNode *node, NSUInteger idx, BOOL *stop) {
        if ([node.name isEqualToString:kNoteToggleName]) { node.alpha = alpha; }
    }];
}

- (void)flashArrow:(SKSpriteNode *)arrow {
    NSUInteger index = [self.arrows indexOfObjectIdenticalTo:arrow] + 1;
    
    // no flashy arrow
    if (self.state == ConfigStateNone) {
        SKAction *flashing = [SKAction sequence:@[
            [SKAction fadeAlphaTo:0.2 duration:0.2], [SKAction fadeAlphaTo:1.0 duration:0.2]
        ]];
        [arrow runAction:[SKAction repeatActionForever:flashing]];
        
        self.state = index;
    }
    
    // cancel/delete note if flashing flashy arrow
    else if (self.state == index) {
        [arrow removeAllActions];
        [arrow runAction:[SKAction fadeAlphaTo:0.2 duration:0.1]];
        
        // reset note index
        arrow.color = [SKColor whiteColor];
        self.noteIndexes[index-1] = @0;
        self.state = ConfigStateNone;
    }
}

- (void)selectNote:(SKShapeNode *)note {
    // play talempong note
    [self playTalempongWithNote:note];

    NSUInteger index;
    if (self.state == ConfigStateHighlightedFirstArrow) {
        index = 0;
    } else if (self.state == ConfigStateHighlightedSecondArrow) {
        index = 1;
    } else {
        return;
    }
    
    // coloring arrow
    SKSpriteNode *arrow = self.arrows[index];
    [arrow removeAllActions];
    [arrow runAction:[SKAction fadeAlphaTo:1.0 duration:0.1]];
    
    // set note index
    self.noteIndexes[index] = note.userData[kNoteKey];
    self.state = ConfigStateNone;
}

- (void)playTalempongWithNote:(SKShapeNode *)note {
    // play talempong
    NSUInteger index = [note.userData[kNoteKey] integerValue];
    [[AudioEngine sharedEngine] previewTalempongNote:index];
    
    // animate pulsing
    CGFloat duration = 0.2;
    CGFloat offset = 5;
    [note runAction:[SKAction sequence:@[
        [SKAction group:@[[SKAction scaleTo:1.2 duration:duration], [SKAction moveByX:-offset y:-offset duration:duration]]],
        [SKAction group:@[[SKAction scaleTo:1 duration:duration], [SKAction moveByX:offset y:offset duration:duration]]]
    ]]];
}

- (void)selectRange:(SKShapeNode *)rangePicker {
    [[[AudioEngine sharedEngine] talempongs] enumerateObjectsUsingBlock:^(Talempong *talempong, NSUInteger idx, BOOL *stop) {
        talempong.noteRange = [rangePicker.userData[kPickerIndexKey] integerValue];
    }];
}

#pragma mark - touch events

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    CGPoint location = [touch locationInNode:self];
    SKNode *node = [self nodeAtPoint:location];
    
    // tapping arrow
    if ([node.name isEqualToString:kArrowName]) {
        [self flashArrow:(SKSpriteNode *)node];
    }
    
    // tapping note
    else if ([node.name isEqualToString:kNoteToggleName]) {
        [self selectNote:(SKShapeNode *)node];
    }
    
    // tapping range picker
    else if ([node.name isEqualToString:kPickerName]) {
        [self selectRange:(SKShapeNode *)node];
    }
    
    [self updateUI];
}

#pragma mark - interfaces

- (void)updateRangePicker {
    __block Talempong *talempong = (Talempong *)self.talempongs[0];
    [[self children] enumerateObjectsUsingBlock:^(SKShapeNode *picker, NSUInteger idx, BOOL *stop) {
        if ([picker.name isEqualToString:kPickerName]) {
            // control opacity
            picker.alpha = ([picker.userData[kPickerIndexKey] integerValue] != talempong.noteRange)? 0.5 : 1.0;
        }
    }];
}

- (void)updateArrows {
    // coloring arrows
    for (int i = 0; i < 2; i++) {
        SKSpriteNode *arrow = self.arrows[i];
        NSInteger note = [self.noteIndexes[i] integerValue];
        
        if (note != 0) {
            arrow.color = [SKColor colorForNote:note];
            arrow.alpha = 1.0;
        } else {
            arrow.color = [SKColor whiteColor];
            arrow.alpha = 0.2;
        }
    }
}

#pragma mark - public methods

- (void)showInScene:(SKScene *)scene {
    if (self.parent) return;
    
    // update noteIndexes
    Talempong *talempong1 = self.talempongs[0];
    Talempong *talempong2 = self.talempongs[1];
    self.noteIndexes = [@[@(talempong1.note), @(talempong2.note)] mutableCopy];
    
    // update position
    self.position = CGPointMake(CGRectGetMidX(scene.frame), CGRectGetMidY(scene.frame));
    self.zPosition = 100;
    [scene addChild:self];
    
    // update UI
    [self updateUI];
}

- (void)hide {
    // update engine
    [[AudioEngine sharedEngine] setTalempongWithNotes:self.noteIndexes];
    [self removeFromParent];
}

- (void)updateUI {
    // update picker
    [self updateRangePicker];
    
    // update arrows
    [self updateArrows];
    
    // update label and toggle
    switch (self.state) {
        case ConfigStateNone: {
            self.taskLabel.text = @"Tap arrow to select its note";
            [self activateToggle:NO];
            break;
        }
        case ConfigStateHighlightedFirstArrow: {
            self.taskLabel.text = @"Select 1st note by tapping a color";
            [self activateToggle:YES];
            break;
        }
        case ConfigStateHighlightedSecondArrow: {
            self.taskLabel.text = @"Select 2nd note by tapping a color";
            [self activateToggle:YES];
            break;
        }
            
        default: break;
    }
}

@end
