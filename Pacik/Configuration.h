//
//  Configuration.h
//  Pacik
//
//  Created by Ikhsan Assaat on 9/2/14.
//  Copyright (c) 2014 Ikhsan Assaat. All rights reserved.
//

@import SpriteKit;

@interface Configuration : SKNode

+ (instancetype)sharedConfig;
- (void)showInScene:(SKScene *)scene;
- (void)hide;

@end
