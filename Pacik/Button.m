//
//  Button.m
//  Pacik
//
//  Created by Ikhsan Assaat on 9/2/14.
//  Copyright (c) 2014 Ikhsan Assaat. All rights reserved.
//

#import "Button.h"

NSString *const kButtonName = @"Button";

@implementation Button

+ (instancetype)createButtonWithText:(NSString *)text {
    Button *button = [Button new];
    button.text = text;
    
    return button;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.fontName = @"Avenir-Black";
        self.fontSize = 100;
        self.fontColor = [SKColor whiteColor];
        self.name = kButtonName;
    }
    return self;
}

@end
