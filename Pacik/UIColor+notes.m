//
//  UIColor+notes.m
//  Pacik
//
//  Created by Ikhsan Assaat on 9/2/14.
//  Copyright (c) 2014 Ikhsan Assaat. All rights reserved.
//

#import "UIColor+notes.h"
#import "Colours.h"

@implementation UIColor (notes)

+ (UIColor *)colorForNote:(NSUInteger)note {
    if (note < 1 || note > 6) return nil;
    
    NSArray *color = @[
        [UIColor watermelonColor],
        [UIColor waveColor],
        [UIColor seafoamColor],
        [UIColor bananaColor],
        [UIColor peachColor],
        [UIColor pastelPurpleColor]
    ];
    return color[note-1];
}


@end
