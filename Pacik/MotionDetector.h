//
//  MotionDetector.h
//  Pacik Lab
//
//  Created by Ikhsan Assaat on 8/26/14.
//  Copyright (c) 2014 Ikhsan Assaat. All rights reserved.
//

@import Foundation;

@class Hit;

@interface MotionDetector : NSObject

@property (nonatomic) NSInteger bufferWidth;

- (void)startWithDetectionBlock:(void(^)(Hit *hit))detectionBlock;
- (void)stop;

@end
