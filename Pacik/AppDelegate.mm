//
//  AppDelegate.m
//  Pacik
//
//  Created by Ikhsan Assaat on 9/2/14.
//  Copyright (c) 2014 Ikhsan Assaat. All rights reserved.
//

#import "AppDelegate.h"
#import "Stk.h"
#import "AudioEngine.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

- (void)setupRawwavePath {
    NSString *path = [[NSBundle mainBundle] pathForResource:@"rawwaves" ofType:@"bundle"];
    stk::Stk::setRawwavePath(path.UTF8String);
}

- (void)setupAudioEngine {
    [AudioEngine sharedEngine];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [self setupRawwavePath];
    [self setupAudioEngine];
    
    return YES;
}

@end
