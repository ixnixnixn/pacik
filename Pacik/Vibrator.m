//
//  Vibrator.m
//  Pacik Lab
//
//  Created by Ikhsan Assaat on 8/26/14.
//  Copyright (c) 2014 Ikhsan Assaat. All rights reserved.
//

#import "Vibrator.h"
#import <AudioToolbox/AudioToolbox.h>

@implementation Vibrator


+ (void)vibrate {
    AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
}

+ (void)vibrateWithAmplitude:(double)amplitude {
    NSDictionary *vibrations = @{
        @"VibePattern": @[@YES, @(amplitude * (150 - 20) + 20), @NO, @0],
        @"Intensity": @1
    };
    AudioServicesPlaySystemSoundWithVibration(4095, nil, vibrations);
}

@end
