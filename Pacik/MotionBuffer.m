//
//  MotionBuffer.m
//  Pacik Lab
//
//  Created by Ikhsan Assaat on 8/26/14.
//  Copyright (c) 2014 Ikhsan Assaat. All rights reserved.
//

#import "MotionBuffer.h"

@import CoreMotion;
#import "Hit.h"

@interface MotionBuffer ()

@property (nonatomic, strong) NSMutableArray *buffer;
@property (nonatomic, strong) NSMutableArray *motions;

@end

@implementation MotionBuffer

- (instancetype)init {
    self = [super init];
    if (self) {
        self.buffer = [NSMutableArray new];
        self.motions = [NSMutableArray new];
        
        self.width = 3;
        self.featureSelectionBlock = ^(CMDeviceMotion *motion) { return @(motion.rotationRate.z); };
    }
    return self;
}

- (BOOL)isFull {
    return (self.buffer.count == self.width);
}

- (void)push:(CMDeviceMotion *)motion {
    if (self.motions.count >= self.width) {
        [self.motions removeObjectAtIndex:0];
        [self.buffer removeObjectAtIndex:0];
    }
    
    [self.motions addObject:motion];
    [self.buffer addObject:self.featureSelectionBlock(motion)];
}

- (Hit *)detectHit {
    
    double threshold = 5.0;
    __block Hit *hit;
    
    // Peak Detection
    
    int peakDetectionType = 1;
    switch (peakDetectionType) {
        case 1:{
            
            // 1 neighbor check
            
            if (self.isFull) {
                double a = [self.buffer[self.width-3] doubleValue];
                double b = [self.buffer[self.width-2] doubleValue];
                double c = [self.buffer[self.width-1] doubleValue];
                
                if (b > a && b > c && b > threshold) {
                    hit = [Hit new];
                    
                    CMDeviceMotion *motion = self.motions[self.width-2];

//                    double energy = pow(motion.userAcceleration.x, 2) + pow(motion.userAcceleration.y, 2) + pow(motion.userAcceleration.z, 2);
//                    double amp = (energy - 2.0) / 10.0;
//                    double energy = motion.rotationRate.z;
//                    double amp = (energy - 5.0) / 20.0;
                    
                    double energy = pow(motion.rotationRate.x, 2) + pow(motion.rotationRate.y, 2) + pow(motion.rotationRate.z, 2);
                    double amp = (energy - 40.0) / 400.0;
                    
                    hit.amplitude =  MIN(amp, 0.98);
                    hit.note = [self classifyNote:motion];
                }
            }
            
            break;
        }
        default:
            break;
    }
    
    return hit;
}

- (NSInteger)classifyNote:(CMDeviceMotion *)motion {
    if (motion.gravity.x > -0.3 ) {
        return 1;
    }
    
    return 2;
}

@end
