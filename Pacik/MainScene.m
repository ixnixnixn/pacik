//
//  GameScene.m
//  Pacik
//
//  Created by Ikhsan Assaat on 9/2/14.
//  Copyright (c) 2014 Ikhsan Assaat. All rights reserved.
//

#import "MainScene.h"
#import "Button.h"
#import "TalempongNode.h"
#import "Configuration.h"
#import "Colours.h"
#import "UIColor+notes.h"
#import "AudioEngine.h"
#import "Talempong.h"
#import "TalempongPreset.h"
#import "MotionDetector.h"
#import "Hit.h"
#import "Vibrator.h"

static NSString *const kSaveButtonName = @"SaveButton";
static NSString *const kLoadButtonName = @"LoadButton";
static NSInteger const kSaveSheetTag = 10;
static NSInteger const kLoadSheetTag = 11;

@interface MainScene() <UIActionSheetDelegate>

@property (nonatomic, weak) TalempongNode *firstNode;
@property (nonatomic, weak) TalempongNode *secondNode;
@property (nonatomic, weak) TalempongNode *selectedNote;
@property (nonatomic, weak) Button *button;

@property (nonatomic, strong) NSTimer *previewTimer;
@property (nonatomic, strong) MotionDetector *motionDetector;
@property (nonatomic) NSUInteger activeTalempongCount;
@property (nonatomic, readonly) NSArray *talempongs;

@end

@implementation MainScene

- (NSArray *)talempongs {
    return [[AudioEngine sharedEngine] talempongs];
}

#pragma mark - buttons

- (void)addPlusButton {
    Button *button = [Button createButtonWithText:@"+"];
    button.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMaxY(self.frame) - 100.0);
    [self addChild:button];
    self.button = button;
}

- (void)addPresetsButtons {
    Button *saveButton = [Button createButtonWithText:@"Save"];
    saveButton.fontSize = 45;
    saveButton.name = kSaveButtonName;
    saveButton.position = CGPointMake(100, CGRectGetMaxY(self.frame) - 80.0);
    [self addChild:saveButton];
    
    Button *loadButton = [Button createButtonWithText:@"Load"];
    loadButton.fontSize = 45;
    loadButton.name = kLoadButtonName;
    loadButton.position = CGPointMake(CGRectGetMaxX(self.frame) - 100.0, CGRectGetMaxY(self.frame) - 80.0);
    [self addChild:loadButton];
}

#pragma mark - notes

- (NSUInteger)countNotes {
    __block NSUInteger count = 0;
    
    [[self children] enumerateObjectsUsingBlock:^(SKNode *node, NSUInteger idx, BOOL *stop) {
        if ([node.name isEqualToString:kTalempongName]) {
            count++;
        }
    }];
    
    return count;
}

- (void)configureTalempongNotes {
    BOOL isShowed = ([Configuration sharedConfig].parent != nil);
    CGFloat duration = 0.1;
    if (!isShowed) {
        [[Configuration sharedConfig] showInScene:self];
        
        // update button 'x'
        [self.button runAction:[SKAction group:@[
            [SKAction rotateToAngle:(M_PI / 4) duration:duration],
            [SKAction moveByX:20 y:5 duration:duration]
        ]]];
        
        [self.motionDetector stop];
    } else {
        [[Configuration sharedConfig] hide];
        
        // update button '+'
        [self.button runAction:[SKAction group:@[
            [SKAction rotateToAngle:0 duration:duration],
            [SKAction moveByX:-20 y:-5 duration:duration]
        ]]];
        
        [self updateTalempongNodes];
        [self startDetecting];
    }
}

- (void)previewingStrike:(NSTimer *)timer {
    Talempong *talempong = (Talempong *)timer.userInfo[@"talempong"];
    [talempong strike:.8];
}

#pragma mark - nodes

- (Talempong *)selectTalempongFromNode:(TalempongNode *)node {
    Talempong *talempong = nil;
    if (self.selectedNote == self.firstNode) {
        talempong = self.talempongs[0];
    }
    else if (self.selectedNote == self.secondNode) {
        talempong = self.talempongs[1];
    }
    return talempong;
}

- (void)updateTalempongNodes {
    // remove all nodes
    self.firstNode = nil;
    self.secondNode = nil;
    [self.children enumerateObjectsUsingBlock:^(SKNode *node, NSUInteger idx, BOOL *stop) {
        if ([node.name isEqualToString:kTalempongName]) { [node removeFromParent]; };
    }];
    
    // count active talempong(s)
    self.activeTalempongCount = 0;
    
    // draw talempong
    [self.talempongs enumerateObjectsUsingBlock:^(Talempong *talempong, NSUInteger idx, BOOL *stop) {
        if (talempong.note != 0) {
            
            CGFloat x = (talempong.stickHardness - 0.8) * (CGRectGetMaxX(self.frame) / 0.2);
            CGFloat y = (talempong.vibrato + 128) * (CGRectGetMaxY(self.frame) / 256);
            CGPoint position = CGPointMake(x, y);
            
            TalempongNode *node = [TalempongNode createNoteWithNumber:talempong.note atPoint:position];
            [self addChild:node];
            
            if (idx == 0)
                self.firstNode = node;
            else if (idx == 1)
                self.secondNode = node;
            
            self.activeTalempongCount++;
        }
    }];
}

#pragma mark - Hit detection

- (void)startDetecting {
    if (self.activeTalempongCount == 0) return;
    
    __block Talempong *talempong;
    __block TalempongNode *node;
    
    [self.motionDetector startWithDetectionBlock:^(Hit *hit) {
        // using two talempong
        if (self.activeTalempongCount == 2) {
            if (hit.note == 1) {
                talempong = (Talempong *)self.talempongs[0];
                node = self.firstNode;
            } else if (hit.note == 2) {
                talempong = (Talempong *)self.talempongs[1];
                node = self.secondNode;
            }
        }
        
        else if (self.activeTalempongCount == 1) {
            if (self.firstNode) {
                talempong = (Talempong *)self.talempongs[0];
                node = self.firstNode;
            } else if (self.secondNode) {
                talempong = (Talempong *)self.talempongs[1];
                node = self.secondNode;
            }
        }
        
        // STRIKE!
        [talempong strikeWithHit:hit];
        
        // VIBRATE!
        [Vibrator vibrateWithAmplitude:hit.amplitude];
        
        // PULSE!
        [self pulse:node withHit:hit];
    }];
}

- (void)pulse:(SKSpriteNode *)node withHit:(Hit *)hit {
    SKSpriteNode *pulse = [SKSpriteNode spriteNodeWithImageNamed:@"hit"];
    pulse.size = node.size;
    pulse.position = node.position;
    pulse.colorBlendFactor = node.colorBlendFactor;
    pulse.color = node.color;
    [self addChild:pulse];
    
    // pulse 0..1 -> 2..5
    CGFloat dur = 0.4;
    CGFloat rad = (hit.amplitude * 3) + 2;
    
    [pulse runAction:[SKAction sequence:@[
        [SKAction group:@[
            [SKAction scaleTo:rad duration:dur],
            [SKAction fadeAlphaTo:0 duration:dur]
        ]],
        [SKAction removeFromParent]
    ]]];
}

#pragma mark - presets

- (void)savePreset:(NSInteger)index {
    Talempong *talempong1 = self.talempongs[0];
    if (talempong1 && talempong1.note != 0) {
        [talempong1 saveParametersToPreset:(index * 2)];
    }
    
    Talempong *talempong2 = self.talempongs[1];
    if (talempong2 && talempong2.note != 0) {
        [talempong2 saveParametersToPreset:(index * 2 + 1)];
    }
}

- (void)loadPreset:(NSInteger)index {
    Talempong *talempong1 = self.talempongs[0];
    BOOL loaded1 = [talempong1 applyParametersFromPreset:(index * 2)];
    
    Talempong *talempong2 = self.talempongs[1];
    BOOL loaded2 = [talempong2 applyParametersFromPreset:(index * 2 + 1)];
    
    if (!loaded1 && !loaded2) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Preset" message:@"Preset have not yet set" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        return;
    }
    
    [self updateTalempongNodes];
    [self startDetecting];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (actionSheet.cancelButtonIndex == buttonIndex) return;
    
    switch (actionSheet.tag) {
        
        case kSaveSheetTag: {
            [self savePreset:buttonIndex];
            break;
        }
        case kLoadSheetTag: {
            [self loadPreset:buttonIndex];
            break;
        }
            
        default: break;
    }
}

#pragma mark -

- (void)didMoveToView:(SKView *)view {
    /* Setup your scene here */
    [self addPlusButton];
    [self addPresetsButtons];

    /* Initiate config window */
    [Configuration sharedConfig];
    
    /* Initiate motion detector*/
    self.motionDetector = [MotionDetector new];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    /* Called when a touch begins */
    UITouch *touch = [touches anyObject];
    CGPoint location = [touch locationInNode:self];
    SKNode *node = [self nodeAtPoint:location];
    
    self.selectedNote = nil;
    // tapping plus button
    if ([node.name isEqualToString:kButtonName]) {
        [self configureTalempongNotes];
    }
    
    // select note
    else if ([node.name isEqualToString:kTalempongName]) {
        self.selectedNote = (TalempongNode *)node;
        [self.selectedNote selectNote];
        
        Talempong *talempong = [self selectTalempongFromNode:self.selectedNote];
        NSDictionary *dict = @{@"talempong" : talempong};
        self.previewTimer = [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(previewingStrike:) userInfo:dict repeats:YES];
    }
    
    // save preset
    else if ([node.name isEqualToString:kSaveButtonName]) {
        UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:@"Save parameters to preset bank" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Preset 1", @"Preset 2", @"Preset 3", @"Preset 4", nil];
        sheet.tag = kSaveSheetTag;
        [sheet showInView:self.view];
    }
    
    // load preset
    else if ([node.name isEqualToString:kLoadButtonName]) {
        UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:@"Load parameters to preset bank" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Preset 1", @"Preset 2", @"Preset 3", @"Preset 4", nil];
        sheet.tag = kLoadSheetTag;
        [sheet showInView:self.view];
    }
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    // move note
    if (self.selectedNote) {
        UITouch *touch = [touches anyObject];
        CGPoint positionInScene = [touch locationInNode:self];
        CGPoint previousPosition = [touch previousLocationInNode:self];
        
        CGPoint translation = CGPointMake(positionInScene.x - previousPosition.x, positionInScene.y - previousPosition.y);
        CGPoint newPosition = CGPointMake(self.selectedNote.position.x + translation.x, self.selectedNote.position.y + translation.y);
        [self.selectedNote setPosition:newPosition];
        
        Talempong *talempong = [self selectTalempongFromNode:self.selectedNote];
        talempong.vibrato = (self.selectedNote.position.y / (CGRectGetMaxY(self.frame) / 256)) - 128;
        talempong.stickHardness = (self.selectedNote.position.x / (CGRectGetMaxX(self.frame) / 0.2)) + 0.8;
    }
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    if (self.selectedNote) {
        [self.selectedNote deselectNote];
        self.selectedNote = nil;
        [self.previewTimer invalidate];
    }
}

- (void)update:(CFTimeInterval)currentTime {
    /* Called before each frame is rendered */
}

@end
