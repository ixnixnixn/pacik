//
//  MotionBuffer.h
//  Pacik Lab
//
//  Created by Ikhsan Assaat on 8/26/14.
//  Copyright (c) 2014 Ikhsan Assaat. All rights reserved.
//

@import Foundation;

@class CMDeviceMotion;
@class Hit;

@interface MotionBuffer : NSObject

@property (nonatomic, copy) NSNumber *(^featureSelectionBlock)(CMDeviceMotion *motion);
@property (nonatomic, readonly) BOOL isFull;
@property (nonatomic) NSUInteger width;

- (void)push:(CMDeviceMotion *)motion;
- (Hit *)detectHit;

@end
