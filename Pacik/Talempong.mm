//
//  Talempong.m
//  Pacik Lab
//
//  Created by Ikhsan Assaat on 8/26/14.
//  Copyright (c) 2014 Ikhsan Assaat. All rights reserved.
//

#import "Talempong.h"

#import "Modalbar.h"
#import "SKINImsg.h"
#import "AEBlockChannel.h"
#import "Hit.h"
#import "Realm.h"
#import "TalempongPreset.h"

@interface Talempong ()

@property (nonatomic) stk::ModalBar *talempong;

@end


@implementation Talempong

- (double)frequencyForNote:(NSInteger)note {
    // Darlenis, Teti, “Mengenal Musik Tradisional Talempong Pacik Di Minangkabau,” Keteg, 6 (2006), 111
    NSArray *notes = @[@469.16, @531.25, @589.33, @649.26, @703.46, @771.99];
    
    // range calculator
    double multiplier = 1;
    switch (self.noteRange) {
        case TalempongNoteRangeLowest: multiplier = 0.5; break;
        case TalempongNoteRangeLow: multiplier = 0.8; break;
        case TalempongNoteRangeNormal: multiplier = 1; break;
        case TalempongNoteRangeHigh: multiplier = 1.2; break;
        case TalempongNoteRangeHighest: multiplier = 1.5; break;
            
        default: break;
    }
    
    return [notes[note-1] doubleValue] * multiplier;
}

- (instancetype)initWithNote:(NSInteger)note {
    self = [super init];
    if (self) {
        self.talempong = new stk::ModalBar();
        
        self.talempong->setPreset(7);
        self.talempong->setStrikePosition(0.5);
        
        self.stickHardness = 0.9;
        self.vibrato = 0;
        self.noteRange = TalempongNoteRangeNormal;
        
        self.talempong->setFrequency([self frequencyForNote:note]);
    }
    
    return self;
}

- (instancetype)init {
    return [self initWithNote:1];
}

- (void)strikeWithHit:(Hit *)hit {
    if (self.note == 0) return;
    self.talempong->strike(hit.amplitude);
}

- (void)strike:(double)amplitude {
    if (self.note == 0) return;
    self.talempong->strike(0.8);
}

- (void)setNote:(NSUInteger)note {
    if (note > 6) return;
    
    _note = note;
    
    if (note != 0) {
        double freq = [self frequencyForNote:note];
        self.talempong->setFrequency(freq);
    }
}

- (void)setStickHardness:(double)stickHardness {
    _stickHardness = stickHardness;
    self.talempong->setStickHardness(_stickHardness);
}

- (void)setVibrato:(double)vibrato {
    _vibrato = vibrato;
    self.talempong->controlChange(__SK_Balance_, 128.0);
    self.talempong->controlChange(__SK_ModFrequency_, fabs(_vibrato));
}

- (AEBlockChannel *)channel {
    return [AEBlockChannel channelWithBlock:^(const AudioTimeStamp *time, UInt32 frames, AudioBufferList *audio) {
        for (int i = 0; i < frames; i++) {
            ((float*)audio->mBuffers[0].mData)[i] =
            ((float*)audio->mBuffers[1].mData)[i] = self.talempong->tick();
        }
    }];
}

#pragma mark - saving and loading

- (void)saveParametersToPreset:(NSInteger)presetId {
    RLMRealm *realm = [RLMRealm defaultRealm];
    BOOL didCreate = NO;
    
    TalempongPreset *preset = [TalempongPreset findWithId:presetId];
    if (preset == nil) {
        preset = [TalempongPreset new];
        preset.presetId = presetId;
        didCreate = YES;
    }
    
    [realm beginWriteTransaction];
    preset.note = self.note;
    preset.noteRange = self.noteRange;
    preset.stickHardness = self.stickHardness;
    preset.vibrato = self.vibrato;

    if (didCreate) {
        [realm addObject:preset];
    }
    [realm commitWriteTransaction];
}

- (BOOL)applyParametersFromPreset:(NSInteger)presetId {
    TalempongPreset *preset = [TalempongPreset findWithId:presetId];
    if (preset == nil) return NO;
    
    self.note = preset.note;
    self.noteRange = (TalempongNoteRange)preset.noteRange;
    self.stickHardness = preset.stickHardness;
    self.vibrato = preset.vibrato;
    
    return YES;
}

@end
